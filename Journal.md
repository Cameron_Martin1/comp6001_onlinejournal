# Online Journal Listing - 10006449


## Week One - *10/07/17-14/07/17*

For the option for GUI Language I decided to use **ASP.net**, I decided to use this framework as it incorporates C#, HTML and CSS which are languages I am familiar with. ASP.net is also a sought after skill in the web development industry. With initial setup of my workstation using **Docker** - an application used to run and manage applications side-by-side in isolated containers proved a struggle during the first setup. I feel as after the second time setting up the workstation it was a lot easier and quicker which was good.

I am also using the **Markdown Language** to submit weekly journal entries, the Markdown Language (.md) has so far proved a *simplistic written language* with little syntax.

Overall I feel that this week I have learnt how to set up a ASP.net Project.

## Week Two - *17/07/17-21/07/17*

This week we began to look into **Model View Controller** (MVC) Design Framework - the pattern that seperates an application into 3 components: model, view and controller. As a class we also continued to progress through examples in order to gain confidence in using ASP.net, though I am still not overly confident in trouble shooting this I am learning. I also encountered errors upon startup which cause me to have to start again. Whilst working through the weekly tutorials I am learning and though I am not the best programmer, I am still learning. 

Progression with **Markdown Language** has also come along and I feel as though I am completely confident in using it.


## Week Three - *24/07/17-28/07/17*

After successfully completing task one I am currently working through the second exercise at a slow pace to allow me to understand ASP.net and Docker commands.
I am still experiencing errors and issues though my problem solving is coming along which is good and I have worked out several issues along the way.
ASP.net is proving not only challenging but interesting and at this stage I'm happy with where I am. 

